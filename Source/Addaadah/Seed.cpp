#include <Addaadah/Seed.h>
#include <Addaadah/Duktape/DuktapeWrapper.h>

//Addaadah::Seed::Seed() : duktape(std::make_unique<DuktapeWrapper>()) {}

Addaadah::Seed::Seed() : duktape(new DuktapeWrapper(), [](DuktapeWrapper *wrapper) { delete wrapper; }) {}

void Addaadah::Seed::runJS(const char *jsCode) {
    duktape->runJS(jsCode);
}
