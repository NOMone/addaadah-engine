#include <Addaadah/System/Log.h>

#include <cstdio>

namespace Sys = Addaadah::System;
using namespace std;

void Sys::logI(const char *logTag, const char *text) {
	#ifdef DEBUG
		printf("%s: %s\n", logTag, text);
	#endif
}

void Sys::logW(const char *logTag, const char *text) {
	#ifdef DEBUG
		printf("%s: %s\n", logTag, text);
	#endif
}
void Sys::logE(const char *logTag, const char *text) {
	#ifdef DEBUG
		printf("%s: %s\n", logTag, text);
	#endif
}

