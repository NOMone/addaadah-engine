#pragma once

namespace Addaadah {
    namespace System {
        void logI(const char *logTag, const char *text);
        void logW(const char *logTag, const char *text);
        void logE(const char *logTag, const char *text);
    }
}

#define LOG_TAG "Addaadah"
#ifdef DEBUG
	#define LOG (text) Addaadah::System::log (         text)
	#define LOGI(text) Addaadah::System::logI(LOG_TAG, text)
    #define LOGW(text) Addaadah::System::logW(LOG_TAG, text)
    #define LOGE(text) Addaadah::System::logE(LOG_TAG, text)
#else
	#define LOG (text)
	#define LOGI(text)
    #define LOGW(text)
    #define LOGE(text)
#endif
