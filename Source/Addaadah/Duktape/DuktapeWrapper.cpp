#include <Addaadah/Duktape/duktape.h>
#include <Addaadah/Duktape/DuktapeWrapper.h>
#include <Addaadah/Duktape/Bindings.h>
#include <Addaadah/System/Log.h>

namespace {

    duk_context *createDuktapeContext() {

        // Create a heap,
        duk_context *duktape_context = nullptr;
        duktape_context = duk_create_heap_default();
        if (duktape_context) {
            LOGE("Duktape heap created.");
        } else {
            LOGE("Failed to create a Duktape heap.\n");
        }

        return duktape_context;
    }

    void destroyDuktapeContext(duk_context *duktape_context) {

        // Destroy the heap,    
        duk_destroy_heap(duktape_context);

        LOGE("Duktape heap destroyed.");
    }
}

Addaadah::DuktapeWrapper::DuktapeWrapper() : duktapeContext(createDuktapeContext(), destroyDuktapeContext) {

    // Register Duktape bindings,
    registerDuktapeBindings(duktapeContext.get());
}

void Addaadah::DuktapeWrapper::runJS(const char *jsCode) {
    
    duk_context *dt_context = duktapeContext.get();
    if (duk_peval_string(dt_context, jsCode)) {
        char message[2048];
        sprintf(message, "addh_runJS failed: %s", duk_safe_to_string(dt_context, -1));
        LOGE(message);
    }
    duk_pop(dt_context);
}
