#pragma once

#include <memory>

typedef struct duk_hthread duk_context;

namespace Addaadah {

    class DuktapeWrapper {
        std::unique_ptr<duk_context, void(*)(duk_context*)> duktapeContext;
    public:
        
        DuktapeWrapper();
        void runJS(const char *jsCode);
    };
}
