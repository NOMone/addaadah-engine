//TODO: we need an automatic bingings generator... 

#include <Addaadah/System/Log.h>

namespace Sys = Addaadah::System;

namespace {

    duk_ret_t dt_log(duk_context *ctx) {
        const char *text;
        text = duk_require_string(ctx, 0);
        LOGE(text);
        return 0;
    }

    duk_ret_t dt_logI(duk_context *ctx) {
        const char *logTag = duk_require_string(ctx, 0);
        const char *text   = duk_require_string(ctx, 1);
        Sys::logI(logTag, text);
        return 0;
    }

    duk_ret_t dt_logW(duk_context *ctx) {
        const char *logTag = duk_require_string(ctx, 0);
        const char *text   = duk_require_string(ctx, 1);
        Sys::logW(logTag, text);
        return 0;
    }

    duk_ret_t dt_logE(duk_context *ctx) {
        const char *logTag = duk_require_string(ctx, 0);
        const char *text   = duk_require_string(ctx, 1);
        Sys::logE(logTag, text);
        return 0;
    }

    void registerLogFunctions(duk_context *ctx) {

        // log,
        duk_push_c_function(ctx, dt_log, 1);
        duk_put_prop_string(ctx, -2, "log");

        // logI,
        duk_push_c_function(ctx, dt_logI, 2);
        duk_put_prop_string(ctx, -2, "logI");

        // logW,
        duk_push_c_function(ctx, dt_logW, 2);
        duk_put_prop_string(ctx, -2, "logW");

        // logE,
        duk_push_c_function(ctx, dt_logE, 2);
        duk_put_prop_string(ctx, -2, "logE");
    }
}
