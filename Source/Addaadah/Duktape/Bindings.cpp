#include <Addaadah/Duktape/duktape.h>
#include <Addaadah/Duktape/Bindings.h>

// Include the bindings files,
#include <Addaadah/Duktape/Bindings/Log.cpp>

void Addaadah::registerDuktapeBindings(duk_context *dt_context) {

    // Create a System object,
    duk_eval_string(dt_context, "var System={}; System;");

    // Register the functions,
    registerLogFunctions(dt_context);
    
    // Pop the System object,
    duk_pop(dt_context);
}
