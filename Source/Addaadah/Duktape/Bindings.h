#pragma once

typedef struct duk_hthread duk_context;

namespace Addaadah {
    void registerDuktapeBindings(duk_context *dt_context);
}

