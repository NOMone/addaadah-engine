#pragma once

#include <memory>

namespace Addaadah {

    class DuktapeWrapper;

    class Seed {
        
        std::unique_ptr<DuktapeWrapper, void(*)(DuktapeWrapper*)> duktape;  // Declared this way to avoid having to include DuktapeWrapper.h

    public:
        
        Seed();
        void runJS(const char *jsCode);
    };
}
